# ssm-demo

#### 介绍
Spring + SpringMVC + Mybatis Maven多模块项目demo

#### 说明

demo.sql文件为数据库建库脚本（Mysql）

建好数据库后，可在项目根目录下使用`mvn package`命令进行打包，在`\ssm-web\target\`目录下，会生成`ssm-demo.war`包；

将war包复制到Tomcat容器中，启动tomcat，访问`localhost:端口/ssm-demo`即可；



联系邮箱：[zzti.zg@gmail.com](zzti.zg@gmail.com)