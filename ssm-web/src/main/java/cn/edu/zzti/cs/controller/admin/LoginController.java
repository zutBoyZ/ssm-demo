package cn.edu.zzti.cs.controller.admin;


import cn.edu.zzti.cs.service.student.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@Controller
@RequestMapping(value = "login")
public class LoginController {

    private StudentService studentService;

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @RequestMapping(value = "submit",method = RequestMethod.POST,produces = "application/json;charset=utf-8")
    public String loginSubmit(@RequestParam("userNum")String userNum,
                              @RequestParam("password")String password,
                              HttpServletRequest request,
                              HttpServletResponse response){
        Map<String, Object> map = studentService.loginAuthenticate(userNum, password);
        if ((boolean)map.get("flag")){
            //  认证成功，保存session
            HttpSession session = request.getSession();
            session.setAttribute("user",map.get("student"));
            //  重定向到主页面
            return "redirect:/html/index.html";
        } else {
            //  认证失败
            response.setContentType("text/html;charset=utf-8");

            try {
                response.getWriter().write("<script>alert('账号或密码错误');history.back()</script>");
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                return null;
            }
        }
    }
}
