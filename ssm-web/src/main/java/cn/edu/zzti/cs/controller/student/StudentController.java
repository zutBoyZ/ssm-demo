package cn.edu.zzti.cs.controller.student;

import cn.edu.zzti.cs.entity.student.Student;
import cn.edu.zzti.cs.service.student.StudentService;
import org.apache.ibatis.annotations.Insert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class StudentController {

    private StudentService studentService;

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @ResponseBody
    @RequestMapping(value = "/getAll",method = RequestMethod.GET,produces = "application/json;charset=utf-8")
    public List<Student> getAllStudent(){
        return studentService.getAllStudent();
    }

}
