package cn.edu.zzti.cs.service.student;

import cn.edu.zzti.cs.entity.student.Student;

import java.util.List;
import java.util.Map;

public interface StudentService {

    List<Student> getAllStudent();

    Map<String,Object> loginAuthenticate(String userNum, String password);

}
