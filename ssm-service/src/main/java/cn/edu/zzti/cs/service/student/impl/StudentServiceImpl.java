package cn.edu.zzti.cs.service.student.impl;

import cn.edu.zzti.cs.dao.student.StudentMapper;
import cn.edu.zzti.cs.entity.student.Student;
import cn.edu.zzti.cs.service.student.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StudentServiceImpl implements StudentService {

    private StudentMapper studentMapper;

    @Autowired
    public void setStudentMapper(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    @Override
    public List<Student> getAllStudent() {
        return studentMapper.getAll();
    }

    @Override
    public Map<String, Object> loginAuthenticate(String userNum, String password) {
        Student stu = studentMapper.getByUserNum(userNum);
        Map<String,Object> map = new HashMap<>();
        map.put("flag",false);
        if (stu!=null && password.equalsIgnoreCase(stu.getStuPwd())){
            map.put("flag",true);
            map.put("student",stu);
        }
        return map;
    }
}
