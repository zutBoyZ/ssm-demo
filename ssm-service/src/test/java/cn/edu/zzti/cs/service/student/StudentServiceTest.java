package cn.edu.zzti.cs.service.student;

import cn.edu.zzti.cs.dao.student.StudentMapper;
import cn.edu.zzti.cs.entity.student.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@ContextConfiguration(locations = "classpath:/spring/applicationContext-dao.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class StudentServiceTest {

    private StudentMapper studentMapper;
    @Autowired
    public void setStudentMapper(StudentMapper studentMapper){
        this.studentMapper = studentMapper;
    }

    @Test
    public void getAllTest(){
        List<Student> result = studentMapper.getAll();
        for (Student student : result) {
            System.out.println(student);
        }
    }

}
