package cn.edu.zzti.cs.dao.student;

import cn.edu.zzti.cs.entity.student.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StudentMapper {

    /*
     * 使用注解方式定义接口
     * 得到表中所有学生信息
     */
    @Select("select * from t_student")
    List<Student> getAll();

    /*
     * 根据id查询一名学生的信息
     * @param id
     * @return
     */
    Student getById(@Param(value = "id")Integer id);

    /*
     * 插入一个学生信息
     * @param student
     */
    Integer insertStudent(@Param(value = "student")Student student);

    /*
     * 根据userNum查询一名学生的信息
     * @param userNum
     * @return
     */
    @Select("select * from t_student where stu_num = #{userNum}")
    Student getByUserNum(@Param(value = "userNum")String userNum);
}
