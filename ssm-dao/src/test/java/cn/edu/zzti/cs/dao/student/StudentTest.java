package cn.edu.zzti.cs.dao.student;

import cn.edu.zzti.cs.entity.student.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


//  使用SpringJUnit4ClassRunner类进行测试
@RunWith(SpringJUnit4ClassRunner.class)
//  加载Spring应用上下文
@ContextConfiguration(locations = "classpath:spring/applicationContext-dao.xml")
@Transactional
public class StudentTest {

    private StudentMapper studentMapper;

    //  使用set方法注入StudentMapper，也可以直接注入，但推荐这么做
    @Autowired
    public void setStudentMapper(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    @Test
    public void getAllTest(){
        List<Student> result = studentMapper.getAll();
        for (Student student : result) {
            System.out.println(student);
        }
    }

    @Test
    public void getByIdTest(){
        Integer id = 2;
        Student student = studentMapper.getById(id);
        System.out.println(student);
    }


}
